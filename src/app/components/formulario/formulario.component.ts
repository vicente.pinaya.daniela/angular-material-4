import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  formulario!: FormGroup;
  contenidoCajaGrande:string[] = [];


  constructor(private fb:FormBuilder) {
    this.crearFormulario();
   }

   crearFormulario():void{
     this.formulario = this.fb.group({
       cajaGrande:[''],
       cajaPeque:this.fb.array([[]])
     })
   }




  ngOnInit(): void {
  }



  get contenidoPeque(){
    return this.formulario.get('cajaPeque') as FormArray
  }

  get DatosNoValidos(){
    return this.formulario.get('cajaPeque & cajaGrande')?.invalid && this.formulario.get('cajaPeque & cajaGrande')?.touched
  }


  adicionar():void {
    this.contenidoPeque.push(this.fb.control(''))
  }

  eliminarCaja(i:number):void{
    this.contenidoPeque.removeAt(i);
  }

  limpiarCajaPeque(i:number){
    console.log(this.formulario.value.cajaPeque[i]);
    this.formulario.value.cajaPeque[i]
  }

  limpiarCajaGrande():void {
    this.contenidoCajaGrande = ['']
  
    console.log(this.contenidoCajaGrande);  
  }


  guardar():void {
    console.log('Guardando');
    this.contenidoCajaGrande = this.formulario.value.cajaPeque.join('\n')
    
  }

  limpiarTodo():void {
    this.contenidoCajaGrande = ['']
    this.formulario.reset

  }

}
